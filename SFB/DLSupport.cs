﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

public abstract class DLSupport
{
    [DllImport("libdl.so.2")]
    static extern IntPtr dlopen(string filename, int flags);

    [DllImport("libdl.so.2")]
    static extern IntPtr dlsym(IntPtr handle, string symbol);

    [DllImport("libdl.so.2")]
    static extern int dlclose(IntPtr handle);

    protected IntPtr handle;

    public DLSupport(string dir, string lib, LoadFlag flag)
    {
        var path = Path.Combine(dir, lib);
        if (!File.Exists(path))
            throw new FileNotFoundException("Library could not be found!");
        handle = dlopen(path, (int)flag);
        if (handle == IntPtr.Zero)
            throw new DllNotFoundException("Library cannot be loaded with given flag.");
    }

    public DLSupport(string lib, LoadFlag flag)
    {
        handle = dlopen(lib, (int)flag);
        if (handle == IntPtr.Zero)
            throw new DllNotFoundException("Library cannot be found or loaded with given flag.");
    }

    protected T GetFunctionDelegate<T>(string symbol, bool SuppressSymbolError = false) where T:Delegate
    {
        var ptr = dlsym(handle, symbol);
        if (!SuppressSymbolError && ptr == IntPtr.Zero)
            throw new Exception(string.Format("Symbol cannot be found for type: {0}", typeof(T).Name));
        return (T)Marshal.GetDelegateForFunctionPointer(ptr, typeof(T));
    }

    protected IntPtr GetSymbolPointer(string symbol)
    {
        return dlsym(handle, symbol);
    }

    /// <summary>
    /// Should not be invoked unless none of DllImport uses the same library as this handle.
    /// The runtime will fail if you attempt to use DllImported functions that happens to be using the same
    /// handle as this.
    /// </summary>
    protected void DangerousDispose()
    {
        dlclose(handle);
    }
}

public enum LoadFlag : int
{
    RTLD_LAZY = 0x00001,
    /// <summary>
    /// Immediate function call binding.
    /// </summary>
    RTLD_NOW = 0x00002,
    /// <summary>
    /// Mask of binding time value.
    /// </summary>
    RTLD_BINDING_MASK = 0x3,
    /// <summary>
    /// Do not load the object.
    /// </summary>
    RTLD_NOLOAD = 0x00004,
    /// <summary>
    /// Use deep binding.
    /// </summary>
    RTLD_DEEPBIND = 0x00008,

    /// <summary>
    /// If the following bit is set in the MODE argument to `dlopen',
    /// the symbols of the loaded object and its dependencies are made
    /// visible as if the object were linked directly into the program.
    /// </summary>
    RTLD_GLOBAL = 0x00100,

    /// <summary>
    /// Unix98 demands the following flag which is the inverse to RTLD_GLOBAL.
    /// The implementation does this by default and so we can define the
    /// value to zero.
    /// </summary>
    RTLD_LOCAL = 0,

    /// <summary>
    /// Do not delete object when closed.
    /// </summary>
    RTLD_NODELETE = 0x01000
}