﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace SFB
{
    public class LinuxLibImplementation : DLSupport
    {

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void AsyncCallback(string path);

        public delegate void DialogInit_dt();
        public delegate IntPtr DialogOpenFilePanel_dt(string title, string directory, string extension, bool multiselect);
        public delegate void DialogOpenFilePanelAsync_dt(string title, string directory, string extension, bool multiselect, AsyncCallback callback);
        public delegate IntPtr DialogOpenFolderPanel_dt(string title, string directory, bool multiselect);
        public delegate void DialogOpenFolderPanelAsync_dt(string title, string directory, bool multiselect, AsyncCallback callback);
        public delegate IntPtr DialogSaveFilePanel_dt(string title, string directory, string defaultName, string extension);
        public delegate void DialogSaveFilePanelAsync_dt(string title, string directory, string defaultName, string extension, AsyncCallback callback);


        public DialogInit_dt DialogInit;
        public DialogOpenFilePanel_dt DialogOpenFilePanel;
        public DialogOpenFilePanelAsync_dt DialogOpenFilePanelAsync;
        public DialogOpenFolderPanel_dt DialogOpenFolderPanel;
        public DialogOpenFolderPanelAsync_dt DialogOpenFolderPanelAsync;
        public DialogSaveFilePanel_dt DialogSaveFilePanel;
        public DialogSaveFilePanelAsync_dt DialogSaveFilePanelAsync;

        public LinuxLibImplementation() : base("libStandaloneFileBrowser.so", LoadFlag.RTLD_LAZY)
        {
            DialogInit = GetFunctionDelegate<DialogInit_dt>("DialogInit");
            DialogOpenFilePanel = GetFunctionDelegate<DialogOpenFilePanel_dt>("DialogOpenFilePanel");
            DialogOpenFilePanelAsync = GetFunctionDelegate<DialogOpenFilePanelAsync_dt>("DialogOpenFilePanelAsync");
            DialogOpenFolderPanel = GetFunctionDelegate<DialogOpenFolderPanel_dt>("DialogOpenFolderPanel");
            DialogOpenFolderPanelAsync = GetFunctionDelegate<DialogOpenFolderPanelAsync_dt>("DialogOpenFolderPanelAsync");
            DialogSaveFilePanel = GetFunctionDelegate<DialogSaveFilePanel_dt>("DialogSaveFilePanel");
            DialogSaveFilePanelAsync = GetFunctionDelegate<DialogSaveFilePanelAsync_dt>("DialogSaveFilePanelAsync");
        }

        public LinuxLibImplementation(string dir) : base(dir, "libStandaloneFileBrowser.so", LoadFlag.RTLD_LAZY)
        {
            DialogInit = GetFunctionDelegate<DialogInit_dt>("DialogInit");
            DialogOpenFilePanel = GetFunctionDelegate<DialogOpenFilePanel_dt>("DialogOpenFilePanel");
            DialogOpenFilePanelAsync = GetFunctionDelegate<DialogOpenFilePanelAsync_dt>("DialogOpenFilePanelAsync");
            DialogOpenFolderPanel = GetFunctionDelegate<DialogOpenFolderPanel_dt>("DialogOpenFolderPanel");
            DialogOpenFolderPanelAsync = GetFunctionDelegate<DialogOpenFolderPanelAsync_dt>("DialogOpenFolderPanelAsync");
            DialogSaveFilePanel = GetFunctionDelegate<DialogSaveFilePanel_dt>("DialogSaveFilePanel");
            DialogSaveFilePanelAsync = GetFunctionDelegate<DialogSaveFilePanelAsync_dt>("DialogSaveFilePanelAsync");
        }


    }
}
